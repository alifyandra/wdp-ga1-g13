from django import forms
from .models import *
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    email = forms.EmailField
    # fullname = forms.CharField
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

        self.fields['email'].required = True
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

    class Meta:
        model = User 
        fields = [  
            'username', 
            'first_name',
            'last_name',
            'email', 
            'password1', 
            'password2',
        ]

class NewCart(forms.ModelForm):
    class Meta:
        model = Cart
        fields = [
            "buyer",
            "items"
        ]
        
class NewReview(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            "author",
            "star",
            "review_text"
        ]