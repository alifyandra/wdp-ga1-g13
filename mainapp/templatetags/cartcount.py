from django import template
from mainapp.models import Cart

register = template.Library()

@register.simple_tag
def cartcount(request):
    a = Cart.objects.filter(buyer=request.user).count()
    return a