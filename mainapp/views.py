from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import *
from .models import *
from django.contrib.auth.decorators import login_required
from django.core import serializers


# Create your views here.
# def index(request):    
#     return render(request, "index.html",{'title':'TikTokped Home'})
def homeRedirect(request):
    return redirect('/home')

def index(request):
    # if this is a POST request we need to process the form data
    carousel_item = Item.objects.all()[:3]

    return render(request, "index.html", 
                    {"title":'TikTokped Home','car_items':carousel_item,'cartcount':cartcount(request)})

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = RegisterForm()
    
    return render(request,'registration/register.html',{'form':form,'cartcount':cartcount(request)})

def technology(request):
    tech_items = Item.objects.filter(category='Technology')
    return render(request, "technology.html",{'title':'Technology','techitems':tech_items,'cartcount':cartcount(request)})

def fashion(request):
    fash_items = Item.objects.filter(category='Fashion')
    return render(request, "fashion.html",{'title':'Fashion','fashitems':fash_items,'cartcount':cartcount(request)})

def travel(request):
    trav_items = Item.objects.filter(category='Travel')
    return render(request, "travel.html",{'title':'Travel','travitems':trav_items,'cartcount':cartcount(request)})

def itempage(request, pk):
    item = Item.objects.get(id=pk)
    if request.method == "POST":
        buyform = request.POST['buyform']
        reviewform = request.POST['reviewform']
        if buyform.is_valid:
            quantity = request.POST['quantity']
            Cart(buyer=request.user,items=item,total=quantity).save()
            usercart = Cart.objects.filter(buyer=request.user)
            data = serializers.serialize('json', usercart, fields = ('buyer', 'items', 'total'))
            return JsonResponse({'usercart':data})

        return JsonResponse({'message':'error saving data'})
        
    else:
        review = Review.objects.filter(item=item)
        return render(request, 'itempage.html',{'title':item.item_name ,'item':item, 'reviews':review,'cartcount':cartcount(request)})

@login_required
def cartpage(request):
    cartitems = Cart.objects.filter(buyer=request.user)
    return render(request, '',{'title':'Cart', 'cartitems':cartitems,'cartcount':cartcount(request)})

@login_required
def transhistory(request):
    pass
@login_required
def reviews(request):
    pass

@login_required
def cartcount(request):
    usercart = Cart.objects.filter(buyer=request.user)
    cartcount = 0
    for cart in usercart:
        cartcount += cart.total
    return cartcount
