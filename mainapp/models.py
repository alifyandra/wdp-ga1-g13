 # Create your models here.
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


# Create your models here.
class Item(models.Model):
    CATEGORY = (
        ('Fashion','Fashion'),
        ('Technology','Technology'),
        ('Travel','Travel'),
    )

    item_name = models.CharField(max_length=30)
    price = models.IntegerField()
    category = models.CharField(max_length=200, choices=CATEGORY)
    stock = models.IntegerField()
    description = models.TextField()
    image = models.TextField(null=True)
    
    class Meta:
        ordering = ['item_name']
    
    def __str__(self):
        return str(self.item_name)
    
class Cart(models.Model):
    buyer = models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
    items = models.ForeignKey(Item, on_delete=models.CASCADE, null=True, blank=True) # confused whether to use foreign key or manytomany
    total = models.IntegerField(default=1)  # to be edited in views.py

    def __str__(self):
        return f"{self.total} number of {self.items}"
    
    def get_total_item_price(self):
        return self.total * self.items.price
    
class Transaction(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    items = models.ManyToManyField(Cart)
    date = models.DateTimeField(auto_now=True)
    finished = models.BooleanField(default=False) # whether transaction is complete or not
    coupon = models.ForeignKey('Coupon', on_delete=models.SET_NULL, blank=True, null=True)
    
    def __str__(self):
        return f"Purchase made on {self.date} by {self.user.username}"
    
    def total_price(self):
        total = 0
        for x in self.items.all():
            total += x.get_total_item_price() # sum of all items in cart
        discount = 0
        for x in self.coupon.all():
            discount += total*x.discount/100 # price cut by discount%
        return total-discount

class Coupon(models.Model):
    code = models.CharField(max_length=16)
    discount = models.IntegerField()
    expiry = models.DateTimeField()
    minimum_price = models.IntegerField()

    def __str__(self):
        return str(self.code)
    
class Review(models.Model):
    author = models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
    star = models.IntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ]
    )
    review_text = models.TextField()
    item = models.ForeignKey(Item,on_delete=models.CASCADE,null=True)

    def __str__(self):
        return f"{self.star} Star review by {self.author}"