from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('home/', views.index, name="mainapp-index"),
    path('technology/', views.technology, name="mainapp-tech"),
    path('register/', views.register, name="mainapp-register"),
    path('fashion/', views.fashion, name="mainapp-fashion"),
    path('travel/', views.travel, name="mainapp-travel"),
    path('item/<str:pk>/', views.itempage, name="mainapp-itempage")
]
