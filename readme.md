<h1>Web Design and Programming - Group Assignment 1</h1>

<h2>Group 13</h2>
<p>
1. Ahmad Izzudin Alifyandra - 1906316944<br>
2. Anugerah Imanuel Amidjaja - 1906426784<br>
3. Avatar Putra Pertama Azka - 1906426802<br>
4. Jerome Emmanuel - 1906426840<br>
5. Muhammad Naufal Razani - 1906426903<br>
</p>

<h2>Our Website - TikTokped</h2>
http://tiktokped.herokuapp.com/

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/alifyandra/wdp-ga1-g13/badges/master/pipeline.svg)](https://gitlab.com/alifyandra/wdp-ga1-g13/commits/master)
[![coverage report](https://gitlab.com/alifyandra/wdp-ga1-g13/badges/master/coverage.svg)](https://gitlab.com/alifyandra/wdp-ga1-g13/commits/master)

<h2>What are we making?</h2>
<p>
We're creating a website which will serve as a mock online shop. Our website will have features such as:<br>
<ul>
<li>1. An automatic slideshow on the main page to showcase the featured items</li>
<li>2. Convenient categorization of goods and articles</li>
<li>3. An admin-editable database featuring the data type "Item" which will have the properties:</li>
   <ul>
      <li>1. Category</li>
      <li>2. Name</li>
      <li>3. Price</li>
      <li>4. Stock</li>
      <li>5. Description</li>
   </ul>
<li>4. A user-submittable transaction which will list the data containing:</li>
   <ul>
      <li>1. The buyer's name</li>
      <li>2. Items</li>
      <li>3. Coupons</li>
      <li>4. Total amount of the transaction</li>
      <li>5. Date of purchase
   </ul>
<li>5. User-searchable coupons that have the properties:</li>
   <ul>
      <li>1. Coupon code</li>
      <li>2. Discount percentage</li>
      <li>3. Expiry date</li>
      <li>4. Minimum price</li>
   </ul>
<li>6. User-submittable reviews for each item, containing the properties:</li>
   <ul>
      <li>1. Reviewer's name</li>
      <li>2. Stars</li>
      <li>3. Item</li>
      <li>4. Message</li>
      <li>5. Date created</li>
   </ul>
</ul>
</p>

<h2>How's our project so far?</h2>
We made a wireframe here: https://wireframepro.mockflow.com/view/Me75a5fb88f6f76d970773c4e13af60f31582871360637<br>
A high-fidelity mockup here: https://www.figma.com/file/F5mbvWNhFanz572yCkgyfk/TikTokped?node-id=0%3A1<br>
And personas of our websites in the repository, also available here: https://docs.google.com/document/d/1XxEmT7BLvXVGHha6wvVSYvHGrjWaLZiKZ2yfXAsKlmU/edit?usp=sharing<br>